import { exflight } from './exflight';
import { Flight } from '../flight';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  flights : Flight[] = []

  constructor() {
    this.flights = exflight.mflight ;
  }

  getFlights(): Flight[]{
    return this.flights ;
  }

  addFlights(f:Flight): void{
    this.flights.push(f) ;
  }
}
