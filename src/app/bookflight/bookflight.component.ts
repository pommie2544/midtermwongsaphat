import { Flight } from './../flight';
import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageService } from '../info/page.service';

@Component({
  selector: 'app-bookflight',
  templateUrl: './bookflight.component.html',
  template: `
  {{ todayDate | thaidate }} <br>
  {{ todayDate | thaidate:'full' }} <br>
  {{ todayDate | thaidate:'medium' }} <br>
  {{ todayDate | thaidate:'short' }} <br>
  `,
  styleUrls: ['./bookflight.component.css'],
})
export class BookflightComponent implements OnInit {
  flight !: Flight;
  flights !: Flight[] ;
  flightForm !: FormGroup ;
  selectfrom:any = '' ;
  selectto:any = '' ;
  todayDate :Date = new Date()
  submit = false ;

  constructor(private fb : FormBuilder, private PageService: PageService) {
    this.flight = new Flight('','','','',0, this.todayDate,0 ,0, this.todayDate) ;
    this.flights = PageService.getFlights() ;
   }

  ngOnInit(): void {
    this.flightForm = this.fb.group({
      FlightName:['', [Validators.required, Validators.minLength(1)]],
      FlightFrom:[this.selectfrom, [Validators.required]],
      FlightTo:[this.selectto, [Validators.required]],
      FlightType:['', [Validators.required]],
      FlightAdult:['', [Validators.required, Validators.pattern('[0-9]+'), Validators.min(1)]],
      FlightDepart:['', [Validators.required]],
      FlightChild:['', [Validators.required, Validators.pattern('[0-9]+'), Validators.min(0)]],
      FlightBaby:['',[Validators.required, Validators.pattern('[0-9]+'), Validators.min(0)]],
      FlightArrive:['',[Validators.required]]
    }, {validator : this.compareDate('FlightDepart', 'FlightArrive')});
  }

  getFlightPages(){
    this.flights = this.PageService.getFlights();
  }
  onSubmitForm(f:FormGroup): void{
    let form_record = new Flight(f.get('FlightName')?.value,
    f.get('FlightFrom')?.value,
    f.get('FlightTo')?.value,
    f.get('FlightType')?.value,
    f.get('FlightAdult')?.value,
    f.get('FlightDepart')?.value,
    f.get('FlightChild')?.value,
    f.get('FlightBaby')?.value,
    f.get('FlightArrive')?.value,)
    this.PageService.addFlights(form_record) ;

  }
  onSubmit():void{
    this.submit = true ;
    if(this.flightForm.valid){
      console.log("Submit") ;
    }
  }
   compareDate(from: string, to: string){
    return(group: FormGroup): {[key: string]: any}=>{
      let f = group.controls[from] ;
      let t = group.controls[to] ;
      if(f.value > t.value){
        return {
          dates: "Date Depart should be less than Date Arrive"
        };
      }
      return {};
    }
   }
}
