import { MatDatepickerModule } from '@angular/material/datepicker';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PageService } from './info/page.service';


import { AppComponent } from './app.component';
import { BookflightComponent } from './bookflight/bookflight.component';
import { ThaiDatePipe } from './thaidatepipe';

@NgModule({
  declarations: [
    AppComponent,
    BookflightComponent,
    ThaiDatePipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MatDatepickerModule,
  ],
  providers: [PageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
